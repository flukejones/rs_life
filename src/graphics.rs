/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use sdl2::surface::Surface as SdlSurface;
use sdl2::pixels::PixelFormatEnum;
use sdl2::rect::Rect as SdlRect;
use sdl2::pixels::Color as SdlColour;
use game::Game;
use settings::Settings;

/// CellState acts like a buffer containing premade coloured surfaces
///
/// Use this to prevent multiple gfx calls from slowing things down
struct CellState<'a> {
    alive: SdlSurface<'a>,
    new: SdlSurface<'a>,
    dead: SdlSurface<'a>,
}
impl<'a> CellState<'a> {
    /// Takes a `Settings` struct for colours, size
    fn new(s: &Settings) -> CellState {
        let alive = SdlColour::RGB(s.colours.alive.0, s.colours.alive.1, s.colours.alive.2);
        let mut s_alive = SdlSurface::new(
            s.game.cellsize as u32,
            s.game.cellsize as u32,
            PixelFormatEnum::RGB24,
        ).unwrap();
        s_alive.fill_rect(None, alive).ok();

        let new = SdlColour::RGB(s.colours.new.0, s.colours.new.1, s.colours.new.2);
        let mut s_new = SdlSurface::new(
            s.game.cellsize as u32,
            s.game.cellsize as u32,
            PixelFormatEnum::RGB24,
        ).unwrap();
        s_new.fill_rect(None, new).ok();

        let dead = SdlColour::RGB(s.colours.dead.0, s.colours.dead.1, s.colours.dead.2);
        let mut s_dead = SdlSurface::new(
            s.game.cellsize as u32,
            s.game.cellsize as u32,
            PixelFormatEnum::RGB24,
        ).unwrap();
        s_dead.fill_rect(None, dead).ok();

        CellState {
            alive: s_alive,
            new: s_new,
            dead: s_dead,
        }
    }
    fn width(&self) -> u32 {
        self.alive.width()
    }
    fn height(&self) -> u32 {
        self.alive.height()
    }
}

pub struct Graphics<'a> {
    cell_state: CellState<'a>,
    bg: SdlSurface<'a>,
    divisor: i32, //
    spacing: i32,
}
impl<'a> Graphics<'a> {
    pub fn new(s: &'a Settings) -> Graphics<'a> {
        let divisor = s.game.cellsize as i32 + s.game.spacing;
        let (width, height) = (
            s.game.width / divisor as u32,
            s.game.height / divisor as u32,
        );

        /// Set up the Game of rects, these corrospond to each cell and are what we draw to
        let gamec = SdlColour::RGB(s.colours.grid.0, s.colours.grid.1, s.colours.grid.2);
        let mut background = SdlSurface::new(s.game.width, s.game.height, PixelFormatEnum::RGB24)
            .unwrap();
        for x in 0..width {
            for y in 0..height {
                let (sx, sy) = (
                    x as i32 * divisor + s.game.spacing,
                    y as i32 * divisor + s.game.spacing,
                );
                let rect = SdlRect::new(sx, sy, s.game.cellsize, s.game.cellsize);
                background.fill_rect(Some(rect), gamec).unwrap();
            }
        }
        Graphics {
            cell_state: CellState::new(s),
            bg: background,
            divisor: divisor,
            spacing: s.game.spacing,
        }
    }

    /// New SdlRect at Game position
    ///
    /// The position is converted to screen coordinates
    fn cell_rect_at(&self, xy: (i32, i32)) -> SdlRect {
        let (sx, sy) = (
            xy.0 * self.divisor + self.spacing,
            xy.1 * self.divisor + self.spacing,
        );
        SdlRect::new(sx, sy, self.cell_state.width(), self.cell_state.height())
    }

    /// Draws the Game background and then the last_gen on top
    ///
    /// We can easily see what state a cell was in previously, and is now simply
    /// by comparing the current generation to the last, eg;
    ///
    /// * if current - last = 0 -> surviving cell
    /// * if current - last = 1 -> cell died
    /// * if current - last = -1 -> cell is new
    pub fn draw(&self, s: &Settings, mut sf: &mut SdlSurface, game: &Game) {
        self.bg.blit(None, &mut sf, None).unwrap();
        for x in 0..game.current.len() - 1 {
            for y in 0..game.current[0].len() - 1 {
                if game.current[x][y] == 1 {
                    let rect = Some(self.cell_rect_at((x as i32, y as i32)));
                    self.cell_state.alive.blit(None, &mut sf, rect).unwrap();
                }
                if s.game.draw_new {
                    if game.current[x][y] - game.last_gen[x][y] == 1 {
                        let rect = Some(self.cell_rect_at((x as i32, y as i32)));
                        self.cell_state.new.blit(None, &mut sf, rect).unwrap();
                    }
                }
                if s.game.draw_dead {
                    if game.last_gen[x][y] - game.current[x][y] == 1 {
                        let rect = Some(self.cell_rect_at((x as i32, y as i32)));
                        self.cell_state.dead.blit(None, &mut sf, rect).unwrap();
                    }
                }
            }
        }
    }
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

extern crate sdl2;
extern crate num;
extern crate time;

// Some things need to be made public for cargo doc to work with it
/// Stores the settings of the game
pub mod settings;
/// Key and mouse bindings to callbacks to process events
pub mod events;
/// Game logic and handling
pub mod game;
/// Draws the graphics to a surface
pub mod graphics;

use game::*;
use graphics::*;
use settings::Settings;
use sdl2::surface::Surface;
use sdl2::pixels::PixelFormatEnum;
use sdl2::rect::Rect;
use events::Events;

/// The main game loop, all game logic is run and updated here
///
/// * Creates an sdl context using `sdl2::init()`
/// * Initialies the game settings struct
/// * Initialises the SDL `event_pump()` and events struct for processing input/os events
/// * Creates the game window
/// * Initialises the renderer
/// * Updates and processes events
/// * updates the game and displays the result
pub fn main() {
    let benchmark = false;
    let test = true;
    let settings = Settings::new();
    let mut game = Game::new(&settings);

    let sdl_ctx = sdl2::init().unwrap();
    let mut events = Events::new(sdl_ctx.event_pump().unwrap());

    let video_ctx = sdl_ctx.video().unwrap();
    let mut timer = sdl_ctx.timer().unwrap();
    /// Create a window
    let window = video_ctx
        .window("Game of Life",
                settings.video.width as u32,
                settings.video.height as u32)
        .position_centered()
        .opengl()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().accelerated().build().unwrap();
    let texture_creator = canvas.texture_creator();

    let mut tick = timer.ticks();
    let last_count = 1000 / settings.game.speed;

    let mut paused = false;
    let mut game_surface = Surface::new(settings.game.width,
                                        settings.game.height,
                                        PixelFormatEnum::RGB24)
            .unwrap();
    let mut view_surface = Surface::new(settings.video.width as u32,
                                        settings.video.height as u32,
                                        PixelFormatEnum::RGB24)
            .unwrap();
    let mut view_clip = Rect::new(0,
                                  0,
                                  settings.video.width as u32,
                                  settings.video.height as u32);
    let graphics = Graphics::new(&settings);
    let pan_speed = settings.video.pan_speed;

    let start_time = time::PreciseTime::now();
    let duration = time::Duration::seconds(1);

    if benchmark || test {
        let x = game.width / 8;
        let y = game.height / 5;
        for w in 0..8 {
            for h in 0..5 {
                let x = (x * w) as i32;
                let y = (y * h) as i32;
                game.add_pattern((x,y), &settings.patterns[0]);
            }
        }

    }

    loop {
        if !benchmark {
            events.update();
            if events.actions.remove("quit") {
                break;
            }
            // TODO: translate zoom to mouse coords on game
            // this means I probably need to use the /virtual/ game, not screen space
            let game_xy = game.screen_to_game((events.mouse_x + view_clip.x(),
                                               events.mouse_y + view_clip.y()));
            // Use while loops here to prevent multiple cell additions as generations iterate
            if events.actions.contains("insert_cell") {
                game.add_cell(game_xy);
            }
            if events.actions.remove("pattern1") {
                game.add_pattern(game_xy, &settings.patterns[0]);
            }
            if events.actions.remove("pattern2") {
                game.add_pattern(game_xy, &settings.patterns[1]);
            }
            if events.actions.remove("pattern3") {
                game.add_pattern(game_xy, &settings.patterns[2]);
            }
            if events.actions.remove("pattern4") {
                game.add_pattern(game_xy, &settings.patterns[3]);
            }
            if events.actions.remove("pattern5") {
                game.add_pattern(game_xy, &settings.patterns[4]);
            }
            if events.actions.remove("pattern6") {
                game.add_pattern(game_xy, &settings.patterns[5]);
            }
            if events.actions.remove("pattern7") {
                game.add_pattern(game_xy, &settings.patterns[6]);
            }
            if events.actions.remove("pause") {
                paused = !paused;
            }
            if events.actions.remove("pan_right") {
                if view_clip.x() + view_clip.width() as i32 +pan_speed <
                                            game_surface.width() as i32 {
                    let x = view_clip.x() as i32;
                    view_clip.set_x(x + pan_speed);
                }
            }
            if events.actions.remove("pan_down") {
                if view_clip.y() + view_clip.height() as i32 + pan_speed <
                                           game_surface.height() as i32 {
                    let y = view_clip.y() as i32;
                    view_clip.set_y(y + pan_speed);
                }
            }
            if events.actions.remove("pan_left") {
                if view_clip.x() - pan_speed > 0 {
                    let x = view_clip.x() as i32;
                    view_clip.set_x(x - pan_speed);
                }
            }
            if events.actions.remove("pan_up") {
                if view_clip.y() - pan_speed > 0 {
                    let y = view_clip.y() as i32;
                    view_clip.set_y(y - pan_speed);
                }
            }

            if events.actions.remove("zoom_in") {
                if view_clip.width() - 10 > settings.game.cellsize as u32 + 10 {
                    let ratio = settings.video.width as f32 / settings.video.height as f32;
                    let x = view_clip.width() - 10;
                    let y = (x as f32 / ratio) as u32;
                    view_clip.set_width(x);
                    view_clip.set_height(y);
                }
            }
            if events.actions.remove("zoom_out") {
                if view_clip.width() + 10 < game_surface.width() + 10 {
                    let ratio = settings.video.width as f32 / settings.video.height as f32;
                    let x = view_clip.width() + 10;
                    let y = (x as f32 / ratio) as u32;
                    view_clip.set_width(x);
                    view_clip.set_height(y);
                }
            }

            if timer.ticks() - tick > last_count as u32 {
                tick = timer.ticks();
                if paused {
                    graphics.draw(&settings, &mut game_surface, &game);
                } else {
                    game.update();
                    graphics.draw(&settings, &mut game_surface, &game);
                }
                game_surface
                    .blit_scaled(Some(view_clip), &mut view_surface, None)
                    .ok();
                let texture = texture_creator
                    .create_texture_from_surface(&view_surface)
                    .unwrap();
                canvas.copy(&texture, None, None).ok();
                canvas.present();
            }
        } else {
            game.update();
            if benchmark {
                if start_time.to(time::PreciseTime::now()) >= duration {
                    println!("Finished {:?} generations in 1 second", game.generations);
                    println!("with {:?} cells", game.width*game.height);
                    break
                }
            }
        }
    }
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use settings::Settings;
use std::thread::{spawn, JoinHandle};
use std::sync::Arc;

/// The `Game` struct stores a complete game state including cells in
/// every location.
///
/// To prevent many system calls slowing down the program, the background
/// Game is drawn once and stored to a surface in the struct.
///
/// It is entirely possible to create and run multiple game states using Game.
/// Each Game is self contained and draws to any surface given to it, which
/// can then be blitted to a display or other surface
///
pub struct Game {
    //TODO: use the last_gen stored... for what, though?
    pub generations: u32,
    pub last_gen: Vec<Vec<i8>>,
    pub current: Arc<Vec<Vec<i8>>>,
    pub width: u32, // divided Game width
    pub height: u32, // divided Game height
    divisor: i32, //
}
impl Game {
    /// Creates and returns a Game struct used which is one complete game state
    pub fn new(s: &Settings) -> Game {
        let divisor = s.game.cellsize as i32 + s.game.spacing;
        let (width, height) = (
            s.game.width / divisor as u32,
            s.game.height / divisor as u32,
        );

        Game {
            generations: 0,
            last_gen: vec![vec![0; height as usize]; width as usize],
            current: Arc::new(vec![vec![0; height as usize]; width as usize]),
            width: width, // store virtual Game x,y size.
            height: height,
            divisor: divisor,
        }
    }

    /// Insert and draw a cell at location
    ///
    /// Accepts i32 so that a shape can be centered on the mouse position rather
    /// than always progressing right+down (assuming top-left = 0,0)
    pub fn add_cell(&mut self, mut xy: (i32, i32)) {
        if xy.0 < 0 {
            xy.0 = self.width as i32 + xy.0
        };
        if xy.1 < 0 {
            xy.1 = self.height as i32 + xy.1
        };
        if xy.0 >= self.width as i32 {
            xy.0 = xy.0 - self.width as i32
        };
        if xy.1 >= self.height as i32 {
            xy.1 = xy.1 - self.height as i32
        };
        let mut current = Arc::get_mut(&mut self.current).unwrap(); // clone a ref count
        current[xy.0 as usize][xy.1 as usize] = 1;
    }

    /// Calls `add_cell()` multiple times while iterating through the vector of locations
    pub fn add_pattern(&mut self, mouse: (i32, i32), shape: &Vec<(i32, i32)>) {
        for xy in shape {
            self.add_cell((xy.0 + mouse.0, xy.1 + mouse.1));
        }
    }

    /// Convert 1D index number to XY coords
    pub fn index_to_xy(&self, i: usize) -> (i32, i32) {
        (
            (i % self.width as usize) as i32,
            (i / self.width as usize) as i32,
        )
    }

    /// Convert XY coords to 1D index number
    ///
    /// Built in wrap around limits.
    pub fn xy_to_index(&self, mut xy: (i32, i32)) -> usize {
        if xy.0 < 0 {
            xy.0 = self.width as i32 + xy.0
        };
        if xy.1 < 0 {
            xy.1 = self.height as i32 + xy.1
        };
        if xy.0 >= self.width as i32 {
            xy.0 = xy.0 - self.width as i32
        };
        if xy.1 >= self.height as i32 {
            xy.1 = xy.1 - self.height as i32
        };
        (xy.0 + (xy.1 * self.width as i32)) as usize
    }

    /// Convert screen location to Game cell location
    pub fn screen_to_game(&self, xy: (i32, i32)) -> (i32, i32) {
        (xy.0 / self.divisor as i32, xy.1 / self.divisor as i32) // Game coordinates
    }

    /// Update the the next generation and swap *next* to *current* to *last_gen*
    ///
    /// The update function should only be called once per frame
    /// or updates won't be visible
    pub fn update(&mut self) {
        let thread_count = 10;
        let div = (self.current.len() / thread_count) as i32;
        let mut start: i32 = 0;
        let mut end: i32 = div - 1;
        let mut handles: Vec<JoinHandle<Vec<Vec<i8>>>> = Vec::new();

        for n in 1..thread_count + 1 {
            let data_ref = self.current.clone(); // Ref counted immutable
            if n > 1 {
                start = end;
                end += div;
            }
            if n < thread_count {
                handles.push(self.make_thread(data_ref, start, end));
            } else {
                let end = data_ref.len() as i32;
                handles.push(self.make_thread(data_ref, start, end));
            }
        }

        let mut next: Vec<Vec<i8>> = Vec::new();
        next.reserve(self.width as usize);

        for handle in handles {
            next.append(handle.join().as_mut().expect("Join thread error"));
        }

        // Swap the buffers around
        let mut current = Arc::get_mut(&mut self.current).unwrap();
        self.last_gen = current.to_owned();
        *current = next;
        self.generations += 1;
    }

    /// The main thread generator - takes a start + end range
    ///
    /// All threads need to have the same data at the same time or results will vary.
    /// The easiest way to do this is by spawning the threads when needed, giving them
    /// an atomic reference to the data they need to work from, and then waiting for
    /// each to return.
    ///
    /// If a thread is started before another thread, which it relies on for data
    /// then things may get un-synchronised
    fn make_thread(
        &self,
        data_ref: Arc<Vec<Vec<i8>>>,
        start: i32,
        end: i32,
    ) -> JoinHandle<Vec<Vec<i8>>> {
        let (width, height) = (self.width, self.height);
        spawn(move || {
            let mut next: Vec<Vec<i8>> = vec![vec![0; height as usize]; (end-start) as usize];

            let x_limit = |mut x: i32| {
                if x < 0 {
                    x = width as i32 + x
                } else if x >= width as i32 {
                    x = x - width as i32
                }
                x as usize
            };
            let y_limit = |mut y: i32| {
                if y < 0 {
                    y = height as i32 + y
                } else if y >= height as i32 {
                    y = y - height as i32
                }
                y as usize
            };

            /// The only nested loop we should need is to iterate through all
            /// elements. We already know where the neighbours are, so it is much
            /// faster to just add them together in sequence since the vector
            /// location holds either a 1 (alive) or a 0 (dead)
            for (i, x) in (start..end).enumerate() {
                for y in 0..(height as i32) {
                    let nc =
                    data_ref[x_limit(x-1)][y_limit(y+1)] +
                    data_ref[x_limit(x-1)][y_limit(y)] +
                    data_ref[x_limit(x-1)][y_limit(y-1)] +
                    data_ref[x_limit(x)][y_limit(y+1)] +
                    // skip center
                    data_ref[x_limit(x)][y_limit(y-1)] +
                    data_ref[x_limit(x + 1)][y_limit(y + 1)] +
                    data_ref[x_limit(x + 1)][y_limit(y)] +
                    data_ref[x_limit(x + 1)][y_limit(y - 1)];

                    if nc < 2 || nc > 3 {
                        next[i as usize][y as usize] = 0;
                    } else if nc == 3 {
                        next[i as usize][y as usize] = 1;
                    } else if nc == 2 {
                        next[i as usize][y as usize] = data_ref[x as usize][y as usize];
                    }
                }
            }
            return next;
        })
    }
}

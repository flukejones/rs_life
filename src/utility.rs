/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

pub fn open(filename: String) -> result(fs::File) {
    let path = match env::home_dir() {
        Some(dir)   => dir.join(".config/rs_life/"),
        None        => panic!("$HOME environment variable does not exist"),
    };
    fs::File::open(path.join(filename))
}

pub fn open_or_create(filename: String) -> fs::File {
    let path = match env::home_dir() {
        Some(dir)   => dir.join(".config/rs_life/"),
        None        => panic!("$HOME environment variable does not exist"),
    };
    match fs::File::open(path.join(filename)) {
        Ok(file) => file,
        _   => fs::File::create(path.join(file)).expect("Config file error")
    }
}

pub fn pattern_to_vec(pattern: String) -> Vec<(i32,i32)> {
    let openfile = open_or_create(pattern);
    let mut buffer:Vec<String> = Vec::new();
    let mut pattern:Vec<(i32,i32)> = Vec::new();

    // Read in to a vector first so it is easier to work with
    for line in BufReader::new(openfile).lines() {
        let line = line.unwrap();
        if !line.starts_with("!") {
            buffer.push(line);
        }
    }
    let mut x = 0;
    let mut y = 0;
    for line in buffer {
        for ch in line.chars() {
            if ch == 'O' {
                pattern.push( (x,y) );
            }
            x += 1;
        }
        y += 1;
        x = 0;
    }
}

pub fn vec_to_pattern() {
    // TODO
    //
    // example output
    //
    // ..OOO...OOO
    //
    // O....O.O....O
    // O....O.O....O
    // O....O.O....O
    // ..OOO...OOO
    //
    // ..OOO...OOO
    // O....O.O....O
    // O....O.O....O
    // O....O.O....O
    //
    // ..OOO...OOO
    //

}

.PHONY: clean release debug dist zip test deps

default: release

release:
	cargo rustc --release -- -C link-args=-s

debug:
	cargo rustc -- -C link-args=-s

dist: clean deps release
	test -d dist || mkdir dist
	cp target/release/life ./dist/
	cd dist && zip -q -r ../life.zip .

test:
	cargo test

doc:
	cargo doc --release --no-deps

clean:
	cargo clean && \
	rm -rf life.zip lib/* dist/*

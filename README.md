# Game of Life  [![Build Status](https://travis-ci.org/Luke-Nukem/rs_life.svg?branch=master)](https://travis-ci.org/Luke-Nukem/rs_life)

A version of Conway's "Game of Life" written in Rust.

**There are multiple branches**

* `master` = 2D grid Vector + threads
* `1D-threaded` = 1D Vector + threads (also the fastest Rust only version)
* `channels` = Uses 2D Vector + Rust threading and channels between threads
* `MPI` = uses openMP via [rsmpi](https://github.com/bsteinb/rsmpi), you will require anything that rsmpi requires. Run on a single computer with `mpirun cargo run --release`
* `openCL` = uses openCL via [ocl](https://github.com/cogciprocate/ocl), you will require openCL drivers.

Compile with;

    RUSTFLAGS="-C opt-level=3 -C debuginfo=0 -C panic=abort -C link-arg=-s" cargo run --release
    
to produce a very small and fast binary. The `RUSTFLAGS` enforces a few rules on the crates that are built alongside `life`. Of especial note is that `link-arg=-s` strips all debug symbols out.

**Built using Linux**, as such I haven't put any Windows or OSX specific code in. It may or may not work.

![animation](docs/gifs/animated.gif)
![screenshot](docs/screenshots/screenshot2.png)

**Controls**

* `1-6` insert a pattern
* `0` insert "pulsar shuttle" pattern
* `Mouse 1` insert single cell. Hold to draw.
* `P` pause simulation
* `WASD` pan view up/down/left/right

You can pan the view around if you set the grid size to larger than the window size. A zoom in/out is in progress.

`config.toml` is located in `~/.config/rs_life/` and contains a variety of settings to play with. If the file does not exist it is created on start with some defaults.
`bindings.toml` is located in `~/.config/rs_life/`, but isn't very useful yet. Eventually this will store custom key bindings for saved patterns. This is really part of an experiment to see if I can architect a good events system with custom action bindings.

---

[**TODO.md**](TODO.md)

[**LICENSE - GPLv3**](LICENSE)

This is an in-progress project that I hack on to learn Rust and SDL when I have time. Contributions may or may not be accepted - that entirely depends on the direction I feel like going at the time. Raising issues or suggestions are definitely welcome, and you're absolutely welcome to fork the code (it is GPLv3 after all :) )
